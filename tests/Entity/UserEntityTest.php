<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserEntityTest extends KernelTestCase
{
    private const EMAIL_CONSTRAINT_MESSAGE = 'L\'email "UserEntityTest@example" n\'est pas valide';

    private const NOT_BLANK_MESSAGE = 'Veuillez saisir une valeur';

    private const INVALIDE_EMAIL_VALUE = 'UserEntityTest@example';

    private const VALID_EMAIL_VALUE = 'UserEntityTest@example.com';

    private const VALID_PASSWORD_VALUE = 'test';

    private ValidatorInterface $validator;

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = self::bootKernel();

        $this->validator = $kernel->getContainer()->get('validator');
    }

    public function testUserEntityIsValid(): void
    {
        $user = new User();

        $user->setEmail(self::VALID_EMAIL_VALUE)
            ->setPassword(self::VALID_PASSWORD_VALUE);

        $this->getValidationErrors($user, 0);
    }

    public function testUserInvalid(): void
    {
        $user = new User();

        $user->setPassword(self::VALID_PASSWORD_VALUE);

        $this->getValidationErrors($user, 1);
    }

    private function getValidationErrors(User $user, int $numberOfErrors): ConstraintViolationList
    {
        $errors = $this->validator->validate($user);

        self::assertCount($numberOfErrors, $errors);

        return $errors;
    }
}
