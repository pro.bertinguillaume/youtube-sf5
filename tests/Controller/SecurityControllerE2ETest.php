<?php

namespace App\Tests\Controller;

use App\Tests\TestTrait;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Symfony\Component\Panther\PantherTestCase;

class SecurityControllerE2ETest extends PantherTestCase
{
    use TestTrait;

    /**
     * @dataProvider provideInvalidCredentials
     * @param int $attemptNumber
     * @param array $invalidCredentials
     * @param string $flashError
     * @param string $screenshotPath
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function testHCaptchaChallengeMustAppearAfterThreeUnsuccessfulLoginAttempts(
        int $attemptNumber,
        array $invalidCredentials,
        string $flashError,
        string $screenshotPath
    ): void
    {
        if ($attemptNumber === 1) {
            $this->truncateTableBeforeTest('auth_logs');
        }

        $client = static::createPantherClient(
            [
                'browser' => static::FIREFOX,
                'external_base_uri' => 'https://127.0.0.1:8000'
            ],
            [],
            [
                'capabilities' => [
                    'acceptInsecureCerts' => true, // firefox conf
                ]
            ]
        );

//        $client->followRedirect();

        $crawler = $client->request('GET', '/login');

        self::assertSelectorTextContains('h1', 'Me connecter');

        $form = $crawler->selectButton('Se connecter')->form($invalidCredentials);

        $client->submit($form);

        if ($attemptNumber !== 4) {
            self::assertSelectorTextContains('div[class="alert alert-danger"]', $flashError);
        } else {
            try {
                $client->waitFor('iframe', 3);
            } catch (NoSuchElementException $error) {
                throw new NoSuchElementException($error);
            } catch (TimeoutException $error) {
                throw new TimeoutException($error);
            }

            self::assertSelectorAttributeContains('iframe', 'title', 'widget containing checkbox for hCaptcha security challenge');
        }

        $client->takeScreenshot($screenshotPath);
    }

    public function provideInvalidCredentials(): \Generator
    {
        yield [
            1,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-01.png'
        ];

        yield [
            2,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-02.png'
        ];

        yield [
            3,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-01'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-03.png'
        ];

        yield [
            4,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-04'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/hcaptcha-guardian.png'
        ];
    }
}
