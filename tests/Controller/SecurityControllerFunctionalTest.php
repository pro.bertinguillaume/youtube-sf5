<?php

namespace App\Tests\Controller;

use App\Tests\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Dotenv\Dotenv;

class SecurityControllerFunctionalTest extends WebTestCase
{
    use TestTrait;

    /**
     * @dataProvider provideInvalidCredentials
     * @param int $attemptsNumber
     * @param array $invalidCredentials
     * @param string $flashError
     */
    public function testFourOrMoreUnsuccessfulLoginAttemptsMustDisplayAntiSpamFlashbagIfJavascriptIsDisabled(
        int $attemptsNumber,
        array $invalidCredentials,
        string $flashError
    ):void
    {
        $this->makeBadAuthenticationAttempt($attemptsNumber, $invalidCredentials);

        if($attemptsNumber <= 5) {
            self::assertSelectorTextContains('div[class="alert alert-danger"]', $flashError);
//            try {
//
//            } catch (ExpectationFailedException $e) {
//                dd($client->getResponse()->getContent();, $flashError, $e->getMessage(),$attemptsNumber);
//            }
        }
    }

    /**
     * @dataProvider provideInvalidCredentials
     * @param int $attemptsNumber
     * @param array $invalidCredentials
     * @param string $flashError
     */
    public function testBruteForceCheckerMustBlockAnyAuthenticationAttemptAfterFiveFailuresWithNoHcaptchaVerification(
        int $attemptsNumber,
        array $invalidCredentials,
        string $flashError
    ): void
    {
        $dotEnv = new Dotenv();

        $dotEnv->populate([
            'APP_HCAPTCHA_VERIFICATION_TEST' => false
        ]);

        $this->makeBadAuthenticationAttempt($attemptsNumber, $invalidCredentials);

        if ($attemptsNumber >= 6) {
            //$client->getResponse()->getContent();
            self::assertSelectorTextContains('div[class="alert alert-danger"]', $flashError);
        }
    }

    public function provideInvalidCredentials(): \Generator
    {
        yield [
            1,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-01.png'
        ];

        yield [
            2,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-02.png'
        ];

        yield [
            3,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-01'
            ],
            'Invalid credentials.',
            './var/tests/screenshots/invalid-credentials-03.png'
        ];

        yield [
            4,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-04'
            ],
            'La vérification anti-spam a échoué, veuillez réessayer.',
            './var/tests/screenshots/hcaptcha-guardian.png'
        ];

        yield [
            5,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-04'
            ],
            "La vérification anti-spam a échoué, veuillez réessayer.",
            './var/tests/screenshots/hcaptcha-guardian-5.png'
        ];

        yield [
            6,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-06'
            ],
            "Il semblerait que vous ayez oublié vos identifiants. Par mesure de sécurité, vous devez patienter jusqu'à",
            './var/tests/screenshots/hcaptcha-guardian-6.png'
        ];

        yield [
            7,
            [
                'email' => 'test@example.com',
                'password' => 'mauvais-07'
            ],
            "Il semblerait que vous ayez oublié vos identifiants. Par mesure de sécurité, vous devez patienter jusqu'à",
            './var/tests/screenshots/hcaptcha-guardian-6.png'
        ];
    }

    private function makeBadAuthenticationAttempt(int $attemptsNumber, array $invalidCredentials): KernelBrowser
    {
        $client = $this->createClientAndFollowRedirects();

        $crawler = $client->request('GET', '/login');

        self::assertSelectorTextContains('h1', 'Me connecter');

        if ($attemptsNumber === 1) {
            $this->truncateTableBeforeTest('auth_logs');
        }

        $form = $crawler->filter('form[method="POST"]')->form($invalidCredentials);

        $client->submit($form);

        return $client;
    }
}
