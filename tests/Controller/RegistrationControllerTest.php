<?php

namespace App\Tests\Controller;

use App\Tests\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    use TestTrait;

    public function testGetRequestToRegistrationPageReturnSuccessfulResponse(): void
    {
        $this->clientGoesOnRegisterPage();

        self::assertResponseIsSuccessful();

        self::assertSelectorTextContains('h1', 'Créer un compte utilisateur');
    }

    public function testSpamBotsAreNotWelcome(): void
    {
        $client = $this->clientGoesOnRegisterPage();

        $client->submitForm(
            "S'inscrire",
            [
                'registration_form[email]' => 'test@example.com',
                'registration_form[password][first]' => 'badpassword',
                'registration_form[password][second]' => 'badpassword',
                'registration_form[agreeTerms]' => true,
                'registration_form[phone]' => 'boobs',
                'registration_form[fax]' => 'boobies'
            ]
        );

        self::assertResponseStatusCodeSame(403, 'Hi!');

        self::assertRouteSame('app_register');
    }

    public function testMustBeRedirectToTheLoginPageIfTheFormIsValid(): void
    {
        $client = $this->clientGoesOnRegisterPage();

        $this->truncateTableBeforeTest('users');

        $client->submitForm(
            "S'inscrire",
            [
                'registration_form[email]' => 'test2@example.com',
                'registration_form[password][first]' => 'badpassword',
                'registration_form[password][second]' => 'badpassword',
                'registration_form[agreeTerms]' => true,
            ]
        );

        self::assertResponseIsSuccessful();

        self::assertRouteSame('app_login');
    }

    private function clientGoesOnRegisterPage(): KernelBrowser
    {
        $client = $this->createClientAndFollowRedirects();

        $client->request('GET', '/register');

        return $client;

    }
}
