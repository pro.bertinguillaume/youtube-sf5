<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait TestTrait
{
    private function createClientAndFollowRedirects(): KernelBrowser
    {
        $client = static::createClient();

        $client->followRedirects();

        return $client;
    }

    private function truncateTableBeforeTest(string $table): void
    {
        $kernel = self::bootKernel();

        $entityManager = $kernel->getContainer()->get('doctrine')->getManager();

        $connection = $entityManager->getConnection()
            ->executeQuery("TRUNCATE TABLE `{$table}`");

        $entityManager->getConnection()->close();
    }

    private function getURIList(
        KernelBrowser $kernelBrowser,
        bool $isPublicRoute,
        ?string $roleRequired = null
    ): array
    {
        $router = $this->client->getContainer()->get('router');

        $routesWithAllParameters = $router->getRouteCollection()->all();

        $publicURI = [];

        foreach ($routesWithAllParameters as $routeName => $routeParameters) {
            if ($routeParameters->getDefault('_public_access') === $isPublicRoute
                && $roleRequired === $routeParameters->getDefault('_role_required')
                && in_array('GET', $routeParameters->getMethods(), true)
            ) {
                $publicURI[] = $routeParameters->getPath();
            }
        }

        return $publicURI;
    }

    private function authenticateAnUserWithSpecicRole(KernelBrowser $client, string $role): void
    {
        if (!in_array($role, ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_UNKNOW'])) {
            throw new \LogicException("The role specified mus be in array..");
        }

        switch ($role) {
            case 'ROLE_USER':
                $email = 'role_user@example.com';
                break;
            case 'ROLE_ADMIN':
                $email = 'role_admin@example.com';
                break;
            default:
                $email = 'role_unknow@example.com';
                break;
        }

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneBy([
            'email' => $email
        ]);

        if ($user === null) {
            throw new \LogicException('User not found');
        }

        $client->loginUser($user);
    }
}
