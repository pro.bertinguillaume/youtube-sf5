<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

final class ProtectedUrlSmokeTest extends WebTestCase
{
    use TestTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createClientAndFollowRedirects();
    }

    public function testAllPagesLoadedSuccessfully(): void
    {
        $this->authenticateAnUserWithSpecicRole($this->client, 'ROLE_USER');
        $protectedURI = $this->getURIList($this->client, false, 'ROLE_USER');
        $protectedURI[] = '/route-not-exist-or-public';

        $countOfPublicURI = count($protectedURI) - 1;

        $countOfSuccessfulProtectedURI = 0;

        $uriNotLoadedSuccessfully = [];

        foreach ($protectedURI as $uri) {
            $this->client->request('GET', $uri);

            if ($this->client->getResponse()->getStatusCode() === Response::HTTP_OK) {
                $countOfSuccessfulProtectedURI++;
            } else {
                $uriNotLoadedSuccessfully[] = $uri;
            }
        }

//        if (!empty($uriNotLoadedSuccessfully)) {
//            dump($uriNotLoadedSuccessfully);
//        }

        self::assertSame($countOfPublicURI, $countOfSuccessfulProtectedURI);
    }
}
