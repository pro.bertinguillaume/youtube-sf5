<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

final class PublicUrlSmokeTest extends WebTestCase
{
    use TestTrait;

    private KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createClientAndFollowRedirects();
    }

    public function testAllPagesLoadedSuccessfully(): void
    {
        $publicURI = $this->getURIList($this->client, true);
        $publicURI[] = '/route-not-exist-or-not-public';

        $countOfPublicURI = count($publicURI) - 1;

        $countOfSuccessfulPublicURI = 0;

        $uriNotLoadedSuccessfully = [];

        foreach ($publicURI as $uri) {
            $this->client->request('GET', $uri);

            if ($this->client->getResponse()->getStatusCode() === Response::HTTP_OK) {
                $countOfSuccessfulPublicURI++;
            } else {
                $uriNotLoadedSuccessfully[] = $uri;
            }
        }

//        if (!empty($uriNotLoadedSuccessfully)) {
//            dump($uriNotLoadedSuccessfully);
//        }

        self::assertSame($countOfPublicURI, $countOfSuccessfulPublicURI);
    }
}
