<?php

namespace App\EventSubscriber\Form;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CreateSondageFormSubscriber implements EventSubscriberInterface
{
    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        ];
    }

    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();

        $userRoles = $form->getConfig()->getOption('user_roles');

        if (in_array('ROLE_ADMIN', $userRoles, true) === true) {
            $form->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Sauvegarder ce sondage en brouillon',
                    'attr' => [
                        'class' => 'btn btn-warning'
                    ]
                ]
            );
        }
    }
}
