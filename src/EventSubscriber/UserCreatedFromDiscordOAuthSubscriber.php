<?php

namespace App\EventSubscriber;

use App\Event\UserCreatedFromDiscordOAuthEvent;
use App\Service\SendEmail;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreatedFromDiscordOAuthSubscriber implements EventSubscriberInterface
{
    // pas de camelCase pour ce logger afin que le bind se fasse correctement avec monolog.yaml
    private LoggerInterface $discordOauthLogger;
    private SendEmail $sendEmail;

    public function __construct(LoggerInterface $discordOauthLogger, SendEmail $sendEmail)
    {
        $this->discordOauthLogger = $discordOauthLogger;
        $this->sendEmail = $sendEmail;
    }

    /**
     * @return array<string>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            UserCreatedFromDiscordOAuthEvent::SEND_EMAIL_WITH_PASSWORD => 'sendEmailWithPassword'
        ];
    }

    public function sendEmailWithPassword(UserCreatedFromDiscordOAuthEvent $event): void
    {
        $email = $event->getEmail();
        $randomPassword = $event->getRandomPassword();

        $this->sendEmail->send([
            'recipient_email' => $email,
            'subject' => 'Compte utilisateur créé suite à une authentification Discord OAuth',
            'html_template' => 'registration/register_discord_oauth_email.twig.html',
            'context' => [
                'randomPassword' => $randomPassword
            ]
        ]);

        $this->discordOauthLogger->info("L'utilisateur {$email} s'est inscrit via Discord OAuth.");
    }
}
