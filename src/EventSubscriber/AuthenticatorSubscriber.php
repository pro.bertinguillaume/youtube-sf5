<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Repository\AuthLogRepository;
use App\Security\BruteForceChecker;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Http\Event\DeauthenticatedEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuthenticatorSubscriber implements EventSubscriberInterface
{
    private AuthLogRepository $authLogRepository;

    private BruteForceChecker $bruteForceChecker;

    private LoggerInterface $securityLogger;

    private RequestStack $requestStack;

    public function __construct(
        AuthLogRepository $authLogRepository,
        BruteForceChecker $bruteForceChecker,
        LoggerInterface $securityLogger,
        RequestStack $requestStack
    )
    {
        $this->authLogRepository = $authLogRepository;
        $this->bruteForceChecker = $bruteForceChecker;
        $this->securityLogger = $securityLogger;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array<string, string>
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onSecurityAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onSecurityAuthenticationSuccess',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            LogoutEvent::class => 'onSecurityLogout',
            'security.logout_on_change' => 'onSecurityLogoutOnChange',
            SecurityEvents::SWITCH_USER => 'onSecuritySwitchUser'
        ];
    }

    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        ['user_IP' => $userIP] = $this->getRouteNameAndUserIP();
        $securityToken = $event->getAuthenticationToken();
        ['email' => $emailEntered] = $securityToken->getCredentials();

        $this->securityLogger->info("{$userIP} a tenté de s'authentifier avec l'email '{$emailEntered}'");

        $this->bruteForceChecker->addFailedAuthAttempt($emailEntered, $userIP);
    }

    public function onSecurityAuthenticationSuccess(AuthenticationSuccessEvent $event): void
    {
        [
            'route_name' => $routeName,
            'user_IP' => $userIP,
        ] = $this->getRouteNameAndUserIP();

        if (empty($event->getAuthenticationToken()->getRoleNames())) {
            $this->securityLogger->info("{$userIP} est anonyme et est apparu sur la route: {$routeName}");
        } else {

            $securityToken = $event->getAuthenticationToken();

            $userEmail = $this->getUserEmail($securityToken);

            $this->securityLogger->info("onSecurityAuthenticationSuccess {$userIP} est {$userEmail}");
        }
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event): void
    {
        ['user_IP' => $userIP] = $this->getRouteNameAndUserIP();

        /**
         * @var TokenInterface $securityToken
         */
        $securityToken = $event->getAuthenticationToken();

        $userEmail = $this->getUserEmail($securityToken);

        $request = $this->requestStack->getCurrentRequest();

        if ($request && $request->cookies->get('REMEMBERME')) {
            $this->securityLogger->info("Un utilisateur anonyme ({$userIP}) a evolue en entité User ({$userEmail}) grçace à un REMEMBERME cookie.");

            $this->authLogRepository->addSuccessfulAttempt($userEmail, $userIP, true);
        } else {
            $this->securityLogger->info("onSecurityInteractiveLogin {$userIP} est {$userEmail}");

            $this->authLogRepository->addSuccessfulAttempt($userEmail, $userIP);
        }
    }

    public function onSecurityLogout(LogoutEvent $event): void
    {
        /**
         * @var RedirectResponse|null $response
         */
        $response = $event->getResponse();

        /**
         * @var TokenInterface|null $securityToken
         */
        $securityToken = $event->getToken();

        if (!$response || !$securityToken) {
            return;
        }

        ['user_IP' => $userIP] = $this->getRouteNameAndUserIP();
        $userEmail = $this->getUserEmail($securityToken);
        $targetUrl = $response->getTargetUrl();

        $this->securityLogger->info("onSecurityLogout {$userIP} est {$userEmail}, s'est deconnecté, go to {$targetUrl}");
    }

    public function onSecurityLogoutOnChange(DeauthenticatedEvent $event): void
    {

    }

    public function onSecuritySwitchUser(SwitchUserEvent $event): void
    {

    }

    /**
     * Return the user IP and the route's name called by user
     *
     * @return array{route_name: mixed, user_IP: string|null}
     */
    private function getRouteNameAndUserIP(): array
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return [
                'route_name' => 'Inconnue',
                'user_IP' => 'Inconnue',
            ];
        }

        return [
            'route_name' => $request->attributes->get('_route'),
            'user_IP' => $request->getClientIp() ?? 'Inconnue',
        ];
    }

    /**
     * Return email user
     *
     * @param TokenInterface $securityToken
     * @return string
     */
    private function getUserEmail(TokenInterface $securityToken): string
    {
        /**
         * @var User $user
         */
        $user = $securityToken->getUser();

        if ($user instanceof User) {
            return $user->getEmail();
        }
        
        return 'Utilisateur anonyme';
    }
}
