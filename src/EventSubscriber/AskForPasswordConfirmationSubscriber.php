<?php

namespace App\EventSubscriber;

use App\Event\AskForPasswordConfirmationEvents;
use App\Utils\LogoutUserTrait;
use http\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AskForPasswordConfirmationSubscriber implements EventSubscriberInterface
{
    use LogoutUserTrait;

    private RequestStack $requestStack;

    /**
     * @var Session<mixed>
     */
    private Session $session;

    private TokenStorageInterface $tokenStorage;

    /**
     * AskForPasswordConfirmationSubscriber constructor.
     * @param RequestStack $requestStack
     * @param Session<mixed> $session
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        RequestStack $requestStack,
        Session $session,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->requestStack = $requestStack;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array<string>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            AskForPasswordConfirmationEvents::MODAL_DISPLAY => 'onModalDisplay',
            AskForPasswordConfirmationEvents::PASSWORD_INVALID => 'onPasswordInvalid',
            AskForPasswordConfirmationEvents::SESSION_INVALIDATE => 'onSessionIvalidate',
        ];
    }

    public function onModalDisplay(AskForPasswordConfirmationEvents $event): void
    {
        $this->sendJsonResponse();
    }

    public function onPasswordInvalid(AskForPasswordConfirmationEvents $event): void
    {
        $this->sendJsonResponse();
    }

    public function onSessionIvalidate(AskForPasswordConfirmationEvents $event): void
    {
        $this->sendJsonResponse(true);
    }

    private function sendJsonResponse(bool $isUserDeauthenticated = false): void
    {
        if ($isUserDeauthenticated) {
            $request = $this->requestStack->getCurrentRequest();

            if (!$request) {
                return;
            }

            $response = $this->logoutUser(
                $request,
                $this->session,
                $this->tokenStorage,
                'danger',
                'Vous avez été déconnecté par mesure de sécurité car 3 mots de passe invalides ont été saisis lors de la confirmation du mot de passe.',
                false,
                true
            );

            $response->send();
            //necessaire pour ne pas executer la suite du code
            exit;
        }

        $response = new JsonResponse([
            'is_password_confirmed' => false
        ]);

        $response->send();
        //necessaire pour ne pas executer la suite du code
        exit();
    }
}
