<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

class MaintenanceSubscriber implements EventSubscriberInterface
{
    private const AUTHORIZED_IP = '127.0.0.1';

    private Environment $twig;

    private string $maintenanceON;

    public function __construct(Environment $twig, string $maintenanceON)
    {
        $this->twig = $twig;
        $this->maintenanceON = $maintenanceON;
    }

    /**
     * @return array<string, string>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest'
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!file_exists($this->maintenanceON) || $event->getRequest()->getClientIp() === self::AUTHORIZED_IP) {
            return;
        }

        $maintenanceTemplate = $this->twig->render('maintenance/site_under_maintenance.html.twig');

        $response = new Response($maintenanceTemplate, Response::HTTP_SERVICE_UNAVAILABLE);

        $event->setResponse($response);

        $event->stopPropagation();
    }
}
