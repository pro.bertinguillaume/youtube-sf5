<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HoneyPotSubscriber implements EventSubscriberInterface
{
    private LoggerInterface $honeypotLogger;

    private RequestStack $requestStack;

    public function __construct(
        LoggerInterface $honeypotLogger,
        RequestStack $requestStack
    )
    {
        $this->honeypotLogger = $honeypotLogger;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array<string,string>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SUBMIT => 'checkHoneyJar'
        ];
    }

    public function checkHoneyJar(FormEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return;
        }

        $data = $event->getData();

        if (!array_key_exists('phone', $data) || !array_key_exists('fax', $data)) {
            throw new HttpException(400, 'Hi!');
        }

        [
            'phone' => $phone,
            'fax' => $fax
        ] = $data;

        if ($phone !== "" || $fax !== "") {
            $this->honeypotLogger->alert("Tentative de robot spammer. ip : '{$request->getClientIp()}'", ['data' => $data]);
            throw new HttpException(403, 'Hi!');
        }
    }
}
