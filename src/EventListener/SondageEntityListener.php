<?php

namespace App\EventListener;

use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class SondageEntityListener
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function prePersist(
        Sondage $sondage,
        LifecycleEventArgs $lifecycleEventArgs
    ): void
    {
        /**
         * @var User $user
         */
        $user = $this->security->getUser();

        if ($user === null) {
            throw new \LogicException('SEL: User cannot be null here');
        }

        $sondage->setUser($user)
            ->setCreatedAt(new \DateTimeImmutable('now'));
    }

    public function preUpdate(
        Sondage $sondage,
        LifecycleEventArgs $lifecycleEventArgs
    ): void
    {
        $sondage->setEditedAt(new \DateTime('now'));
    }
}
