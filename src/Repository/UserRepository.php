<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getUserFromDiscordOAuth(
        string $discordID,
        string $discordUserName,
        string $email
    ): ?User
    {
        $user = $this->findOneBy([
            'email' => $email
        ]);

        if (!$user) {
            return null;
        }

        if ($user->getDiscordID() !== $discordID) {
            $user = $this->updateUserWithDiscordData($discordID, $discordUserName, $user);
        }

        return $user;
    }

    public function updateUserWithDiscordData(
        string $discordID,
        string $discordUserName,
        User $user
    ): User
    {
        $user->setDiscordID($discordID)
            ->setDiscordUserName($discordUserName);

        $this->_em->flush();

        return $user;
    }

    public function createUserFromDiscordOAuth(
        string $discordID,
        string $discordUserName,
        string $email,
        string $randomPassword
    ): User
    {
        $user = new User();

        $user->setDiscordID($discordID)
            ->setDiscordUserName($discordUserName)
            ->setEmail($email)
            ->setIsVerified(true)
            ->setPassword($randomPassword);

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }
}
