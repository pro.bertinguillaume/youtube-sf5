<?php

namespace App\Repository;

use App\Entity\AuthLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AuthLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthLog[]    findAll()
 * @method AuthLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<AuthLog>
 */
class AuthLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuthLog::class);
    }

    public const BLACK_LISTING_DELAY_IN_MINUTE = 15;

    public const MAX_FAILED_AUTH_ATTEMPS = 5;

    /**
     * Add failed authentification attempt.
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @param bool $isBlackListed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addFailedOfAttempt(string $emailEntered, ?string $userIp, bool $isBlackListed = false): void
    {
        $authAttempt = (new AuthLog($emailEntered, $userIp))->setIsSuccessfulAuth(false);

        if ($isBlackListed) {
            $authAttempt->setStartOfBlackListing(new \DateTimeImmutable('now'))
                ->setEndOfBlackListing(new \DateTimeImmutable(sprintf('+%d minutes', self::BLACK_LISTING_DELAY_IN_MINUTE)));
        }

        $this->_em->persist($authAttempt);

        $this->_em->flush();
    }

    /**
     * Add a successful authentification attempt.
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @param bool $isRememberMeAuth Set to true if user is authentifcated by remember cookie
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addSuccessfulAttempt(
        string $emailEntered,
        ?string $userIp,
        bool $isRememberMeAuth = false
    ): void
    {
        $authAttempt = new AuthLog($emailEntered, $userIp);

        $authAttempt->setIsSuccessfulAuth(true)
            ->setIsRememberMeAuth($isRememberMeAuth);

        $this->_em->persist($authAttempt);

        $this->_em->flush();
    }

    /**
     * Returns the number of recent failed authentication attempt.
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRecentAuthAttemptfailure(string $emailEntered, ?string $userIp): int
    {
        return $this->createQueryBuilder('af')
            ->select('COUNT(af)')
            ->where('af.authAttemptAt >= :datetime')
            ->andWhere('af.userIp = :userIp')
            ->andWhere('af.emailEntered = :emailEntered')
            ->andWhere('af.isSuccessfulAuth = false')
            ->setParameters(new ArrayCollection([
                new Parameter('datetime', new \DateTimeImmutable(sprintf('-%d minutes', self::BLACK_LISTING_DELAY_IN_MINUTE))),
                new Parameter('userIp', $userIp),
                new Parameter('emailEntered', $emailEntered)
            ]))
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Returns whether or not user will be blacklisted after attempt
     * @param string $emailEntered
     * @param string|null $userIp
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isBlackListedWhitThisAttemptFailure(
        string $emailEntered,
        ?string $userIp
    ): bool
    {
        return $this->getRecentAuthAttemptfailure($emailEntered, $userIp) >= self::MAX_FAILED_AUTH_ATTEMPS - 2; //particularité ici, faire attention
    }

    /**
     * Returns the last blacklisted entry of userIp/email pair if exists.
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @return AuthLog|null
     * @throws \Exception
     */
    public function getEmailAndUserIpPairBlacklistedIfExits(
        string $emailEntered,
        ?string $userIp
    ): ?AuthLog
    {
        return $this->createQueryBuilder('bl')
            ->select('bl')
            ->where('bl.userIp = :userIp')
            ->andWhere('bl.emailEntered = :emailEntered')
            ->andWhere('bl.endOfBlackListing IS NOT NULL')
            ->andWhere('bl.endOfBlackListing >= :datetime')
            ->setParameters(new ArrayCollection([
                new Parameter('datetime', new \DateTimeImmutable(sprintf('-%d minutes', self::BLACK_LISTING_DELAY_IN_MINUTE))),
                new Parameter('userIp', $userIp),
                new Parameter('emailEntered', $emailEntered)
            ]))
            ->orderBy('bl.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Returns the end of blacklinsting rounded up to next minute
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @return string|null the time display like: 12h00
     * @throws \Exception
     */
    public function getEndOfBlackLinsting
    (
        string $emailEntered,
        ?string $userIp
    ): ?string
    {
        $blackListing = $this->getEmailAndUserIpPairBlacklistedIfExits($emailEntered, $userIp);

        if (!$blackListing || $blackListing->getEndOfBlackListing() === null) {
            return null;
        }

        return $blackListing->getEndOfBlackListing()->add(new \DateInterval('PT1M'))->format('H\hi');
    }
}
