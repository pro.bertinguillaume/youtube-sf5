<?php

namespace App\Repository;

use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sondage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sondage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sondage[]    findAll()
 * @method Sondage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<Sondage>
 */
class SondageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sondage::class);
    }

    public function getCountOfCreatedSondages(User $user): int
    {
        return $this->createQueryBuilder('sc')
            ->select('COUNT(sc)')
            ->where('sc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getCountOfPublishedSondages(User $user): int
    {
        return $this->createQueryBuilder('sc')
            ->select('COUNT(sc)')
            ->where('sc.user = :user')
            ->andWhere('sc.isPublished = true')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
