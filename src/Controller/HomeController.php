<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"}, defaults={"_public_access": true})
     */
    public function index(): Response
    {
        // TODO Afficher un bouton pour chaque catégorie, le clique sur la catégorie envoie vers la recherche préremplie et charge les 20 premiers sondage

        // TODO récupérer les 5 derniers sondages publiés
        // TODO récupérer les 5 sondages avec le plus de vote
        // TODO si l'utilisateur est connecté, récupérer les 5 derniers sondages
        // TODO si l'utilisateur est connecté, récupérer les 5 derniers sondages des catégories choisies comme interêt

        // TODO présenter les statistiques du site
        // TODO présenter les statistiques du site sur la dernière semaine

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
