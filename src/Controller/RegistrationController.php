<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\SendEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register", methods={"GET","POST"}, defaults={"_public_access": true})
     */
    public function register(
        EntityManagerInterface $entityManager,
        Request $request,
        SendEmail $sendEmail,
        TokenGeneratorInterface $tokenGenerator
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registrationToken = $tokenGenerator->generateToken();

            $user->setRegistrationToken($registrationToken)
                ->setAccountMustBeVerifiedBefore((new \DateTimeImmutable('now'))->add((new \DateInterval("P1D"))))
                ->setIsVerified(false);
            // ->setPassword($passwordEncoder->encodePassword($user,$form->get('password')->getData()));
            // Gérer dans EventPasswordEncoderSubscriber

            $entityManager->persist($user);
            $entityManager->flush();

            /**
             * @var \DateTimeImmutable $accountMustBeVerifiedBefore
             */
            $accountMustBeVerifiedBefore = $user->getAccountMustBeVerifiedBefore();
            $sendEmail->send([
                'recipient_email' => $user->getEmail(),
                'subject' => 'Vérification de votre adresse e-mail',
                'html_template' => 'registration/register_confirmation_email.html.twig',
                'context' => [
                    'userID' => $user->getId(),
                    'registrationToken' => $registrationToken,
                    'tokenLifeTime' => $accountMustBeVerifiedBefore->format('d/m/Y à H:i')
                ]
            ]);

            $this->addFlash('success', "Votre compte utilisateur a bien été créé, veuillez consulter vos mails pour confirmer votre inscription");

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/{token}", name="app_verify_account", methods={"GET"})
     */
    public function verifyUserAccount(
        EntityManagerInterface $entityManager,
        User $user,
        string $token
    ): Response
    {
        /**
         * @var \DateTimeImmutable $accountMustBeVerifiedBefore
         */
        $accountMustBeVerifiedBefore = $user->getAccountMustBeVerifiedBefore();
        if ($user->getRegistrationToken() === null
            || $user->getRegistrationToken() !== $token
            || $this->isNotRequestInTime($accountMustBeVerifiedBefore)
        ) {
            throw new AccessDeniedException();
        }

        $user->setIsVerified(true);
        $user->setAccountVerifiedAt(new \DateTimeImmutable('now'));
        $user->setRegistrationToken(null);

        $entityManager->flush();

        $this->addFlash('success', 'Votre compte utilisateur est activé, vous pouvez vous connecter.');

        return $this->redirectToRoute('app_login');
    }

    private function isNotRequestInTime(\DateTimeImmutable $accountMustBeVerifiedBefore): bool
    {
        return (new \DateTimeImmutable('now') > $accountMustBeVerifiedBefore);
    }
}
