<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Form\CreateSondageFormType;
use App\Repository\SondageRepository;
use App\Security\Voter\SondageVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SondagesController extends AbstractController
{
    /**
     * @Route(
     *     "/sondages/create",
     *     name="app_sondages_create",
     *     methods={"GET", "POST"},
     *     defaults={"_public_access": false}
     *     )
     */
    public function create(
        EntityManagerInterface $entityManager,
        Request $request
    ): Response
    {
        $this->denyAccessUnlessGranted(SondageVoter::CREATE);

        $user = $this->getUser();

        if ($user === null) {
            throw new \LogicException('User ne peut pas être null ici');
        }

        $sondage = new Sondage();

        $form = $this->createForm(
            CreateSondageFormType::class,
            $sondage,
            [
                'user_roles' => $user->getRoles()
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->persistSondage($form, $sondage, $entityManager);

            $entityManager->flush();

            $this->addFlash('success', 'Sondage créé !');

            return $this->redirectToRoute('app_user_account_profile_home');
        }

        return $this->render(
            'sondages/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/sondages/edit/{id<\d+>}",
     *     name="app_sondages_edit",
     *     methods={"GET", "POST"},
     *     defaults={"_public_access": false}
     *     )
     */
    public function edit(
        Sondage $sondage,
        EntityManagerInterface $entityManager,
        Request $request
    ): Response
    {
        $this->denyAccessUnlessGranted(SondageVoter::EDIT, $sondage);

        $user = $this->getUser();

        if ($user === null) {
            throw new \LogicException('User ne peut pas être null ici');
        }

        $form = $this->createForm(
            CreateSondageFormType::class,
            $sondage,
            [
                'is_edition' => true
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * exemple si fichier à upload - gere par le is_edition du form car le field html est vide mais est require à la soumission
             * if($form->get('picture')->getData() !== null) {
             *    $this->persisPicture(...)
             * }
             */


            $entityManager->flush();

            $this->addFlash('success', 'Sondage modifié !');

            return $this->redirectToRoute('app_user_account_profile_home');
        }

        return $this->render(
            'sondages/create.html.twig',
            [
                'form' => $form->createView(),
                'isEdition' => true
            ]
        );
    }

    private function persistSondage(
        FormInterface $form,
        Sondage $sondage,
        EntityManagerInterface $entityManager
    ): void
    {
        $clickedButton = $form->getClickedButton();

        if ($clickedButton === null) {
            throw new \LogicException('No clicked button');
        }

        switch ($clickedButton->getConfig()->getOptions()['label']) {
            case 'Publier le sondage':
                $sondage
                    ->setIsPublished(true)
                    ->setPublishedAt(new \DateTimeImmutable('now'));
                break;
            case 'Sauvegarder ce sondage en brouillon':
                $sondage->setIsPublished(false);
                break;
            default:
                throw new \LogicException('Un label a été modifié.');
        }

        $entityManager->persist($sondage);
    }
}
