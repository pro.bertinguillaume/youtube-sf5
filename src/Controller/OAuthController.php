<?php

namespace App\Controller;

use App\Utils\OAuth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class OAuthController extends AbstractController
{
    use OAuth;

    private const DISCORD_ENDPOINT = 'https://discord.com/api/oauth2/authorize';

    /**
     * @Route("/oauth/discord", name="app_oauth_discord", methods={"GET"})
     */
    public function loginWithDiscord(
        CsrfTokenManagerInterface $csrfTokenManager,
        UrlGeneratorInterface $urlGenerator
    ): RedirectResponse
    {
        // https://discord.com/developers/applications/840966237483958283/oauth2 Redirect
        // https://127.0.0.1:8000/login?discord-oauth-provider=1
        $redirectURL = $this->getDiscordRedirectUrl($urlGenerator);

        $queryParams = http_build_query([
            'client_id' => $this->getParameter('app.discord_client_id'),
            'prompt' => 'content',
            'redirect_uri' => $redirectURL,
            'response_type' => 'code',
            'scope' => 'identify email',
            'state' => $csrfTokenManager->getToken('oauth-discord-oc')->getValue()
        ]);

        return new RedirectResponse(self::DISCORD_ENDPOINT . '?' . $queryParams);
    }
}
