<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Repository\SondageRepository;
use App\Security\AskForPasswordConfirmation;
use App\Utils\LogoutUserTrait;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UserAccountAreaController
 * @package App\Controller
 * @Route("user/account/profile", name="app_user_account_profile_", defaults={"_public_access": false, "_role_required": "ROLE_USER"})
 * @IsGranted("ROLE_USER")
 */
class UserAccountAreaController extends AbstractController
{
    use LogoutUserTrait;

    private AskForPasswordConfirmation $askForPasswordConfirmation;

    private EntityManagerInterface $entityManager;

    /**
     * @var Session<mixed>
     */
    private Session $session;

    /**
     * UserAccountAreaController constructor.
     * @param EntityManagerInterface $entityManager
     * @param Session<mixed> $session
     * @param AskForPasswordConfirmation $askForPasswordConfirmation
     */
    public function __construct(EntityManagerInterface $entityManager, Session $session, AskForPasswordConfirmation $askForPasswordConfirmation)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->askForPasswordConfirmation = $askForPasswordConfirmation;
    }

    /**
     * @Route("", name="home", methods={"GET"})
     */
    public function home(SondageRepository $sondageRepository): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $form = $this->createForm(ResetPasswordType::class, null, [
            'action' => $this->generateUrl('app_user_account_profile_modify_password'),
            'attr' => [
                'class' => 'mt-3'
            ]
        ]);

        return $this->render('user_account_area/index.html.twig', [
            'coutCreatedSondage' => $sondageRepository->getCountOfCreatedSondages($user),
            'countPublishedSondage' => $sondageRepository->getCountOfPublishedSondages($user),
            'modifyPasswordForm' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/toggle-guard-checking-IP", name="toggle_guard_checking_IP", methods={"POST"})
     */
    public function toggleGuardCheckingIp(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(400, 'The request header "X-Requested-With" is missing');
        }

        if ($request->headers->get('Switch-Guard-Checking-IP-Choice')) {
            $switchChoice = $request->getContent();
            if (!in_array($switchChoice, ['true', 'false'], true)) {
                throw new HttpException(400, 'Expected value is "true" or "false"');
            }

            $this->session->set('Switch-Guard-Checking-IP-Choice', $switchChoice);
        }

        $this->askForPasswordConfirmation->ask();

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $switchChoice = $this->session->get('Switch-Guard-Checking-IP-Choice');

        if ($switchChoice === null) {
            throw new HttpException(400, "La valeur du toggle switch est manquante, avez-vous oublié 
            le header 'Switch-Guard-Checking-IP-Choice' lors de l'envoi de votre requête ?");
        }

        $this->session->remove('Switch-Guard-Checking-IP-Choice');

        $isSwitchON = filter_var($switchChoice, FILTER_VALIDATE_BOOLEAN);

        $user->setIsGuardCheckIp($isSwitchON);

        $this->entityManager->flush();

        return $this->json([
            'is_password_confirmed' => true,
            'is_guard_checking_IP' => $isSwitchON
        ]);
    }

    /**
     * @Route("/add-current-IP", name="add_current_IP", methods={"POST"})
     */
    public function addCurrentUserIpToWhiteList(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(400, 'The request header "X-Requested-With" is missing');
        }

        $this->askForPasswordConfirmation->ask();

        $userIP = $request->getClientIp();

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $allIpWhiteListed = array_unique(array_merge($user->getWhitelistedIpAdresses(), [$userIP]));

        $user->setWhitelistedIpAdresses($allIpWhiteListed);

        $this->entityManager->flush();

        return $this->json([
            'is_password_confirmed' => true,
            'user_IP' => implode(' | ', $user->getWhitelistedIpAdresses()),
        ]);
    }

    /**
     * @Route("/edit-user-ip-whitelist", name="edit_user_ip_whitelist", methods={"POST"})
     */
    public function editUserIPWhitelist(Request $request): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(400, 'The request header "X-Requested-With" is missing');
        }

        if ($request->headers->get('User-IP-Entered')) {
            $data = $request->getContent();

            if (!is_string($data)) {
                throw new HttpException(400, "L'adresse IP saisie est incorrecte.");
            }

            $userIpEnteredArray = array_filter(explode(',', $data), function ($userIpEntered) {
                return filter_var($userIpEntered, FILTER_VALIDATE_IP);
            });

            $this->session->set('User-IP-Entered', $userIpEnteredArray);
        }

        $this->askForPasswordConfirmation->ask();

        /**
         * @var User $user
         */
        $user = $this->getUser();

        $userIpEnteredArray = $this->session->get('User-IP-Entered');

        if ($userIpEnteredArray === null) {
            throw new HttpException(400, "Il n'y a pas d'adresses IP à ajouter, avez-vous oublié 
            le header \"User-IP-Entered\" lors de l'envoi de votre requête?");
        }

        $this->session->remove('User-IP-Entered');

        $user->setWhitelistedIpAdresses($userIpEnteredArray);

        $this->entityManager->flush();

        return $this->json([
            'is_password_confirmed' => true,
            'user_IP' => implode(' | ', $userIpEnteredArray)
        ]);
    }

    /**
     * @Route("/modify-password", name="modify_password", methods={"POST"})
     */
    public function modifyPassword(
        Request $request,
        TokenStorageInterface $tokenStorage,
        ValidatorInterface $validator
    ): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(400, 'The request header "X-Requested-With" is missing');
        }

        if ($request->headers->get('Password-Modification')) {
            /**
             * @var string $json
             */
            $json = $request->getContent();

            $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

            if (!array_key_exists('password', $data)) {
                throw new HttpException(400, 'Le mot de passe doit être saisi.');
            }

            $passwordEntered = $data['password'];

            $violations = $validator->validatePropertyValue(User::class, 'password', $passwordEntered);

            if (count($violations) !== 0) {
                throw new HttpException(400, $violations[0]->getMessage());
            }

            $this->session->set('Password-Entered-Modification', $passwordEntered);
        }

        $this->askForPasswordConfirmation->ask();

        $passwordEntered = $this->session->get('Password-Entered-Modification');

        if ($passwordEntered === null) {
            throw new HttpException(400, 'Le mot de passe est manquant, avez-vous oublié le header "Password-Modification" lors de l\'envoi de votre requête');
        }

        /**
         * @var User $user
         */
        $user = $this->getUser();

        // $user->setPassword($passwordEncoder->encodePassword($user, $passwordEntered));
        // Gérer dans EventPasswordEncoderSubscriber
        $user->setPassword($passwordEntered);

        $this->entityManager->flush();

        $response = $this->logoutUser(
            $request,
            $this->session,
            $tokenStorage,
            'success',
            'Votre mot de passe a bien été modifié, vous pouvez à présent vous connecter.',
            true,
            true
        );

        return $response;
    }
}
