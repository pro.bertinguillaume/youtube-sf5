<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Reponse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->generateCategories(2);

        $manager->flush();
    }

    /**
     * @param int $nbCategories
     */
    private function generateCategories(int $nbCategories): void
    {
        for ($i = 0; $i < $nbCategories; $i++) {
            $category = (new Category())->setName("Categorie " . $i);

            $this->addReference("category{$i}", $category);

            $this->manager->persist($category);
        }
    }
}
