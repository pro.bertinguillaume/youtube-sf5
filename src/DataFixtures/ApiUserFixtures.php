<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiUserFixtures extends Fixture
{
    private HttpClientInterface $httpClient;

    private ObjectManager $manager;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->getUsers(3);

        $this->manager->flush();
    }

    private function getUsers(int $nbuser): void
    {
        $randomEmails = $this->fetchRandomUserData($nbuser);
        for ($i = 0; $i < $nbuser; $i++) {
            $modulo = $i % 2;

            $user = new User();

            $user->setEmail($randomEmails[$i]['email'])
                // ->setPassword($this->passwordEncoder->encodePassword($user, "test")) <- EventPasswordEncoderSubscriber
                ->setPassword("test")
                ->setIsVerified(($modulo === 1));

            $this->manager->persist($user);

            $this->addReference("user{$i}", $user);
        }
    }

    /**
     * @param int $numberOfResult
     * @param string $nationality
     * @return array<int, array{email: string}>
     */
    public function fetchRandomUserData(int $numberOfResult = 1, string $nationality = 'fr'): array
    {
        $response = $this->httpClient->request('GET', 'https://randomuser.me/api/', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'query' => [
                'format' => 'json',
                'inc' => 'email',
                'nat' => $nationality,
                'results' => $numberOfResult
            ]
        ]);

        $data = $response->toArray();

        if (!array_key_exists('results', $data)) {
            throw new ServiceUnavailableHttpException("La clé \"results\" n'existe pas dans le tableau récupéré sur https://randomuser.me/api/.");
        }

        return $data['results'];
    }
}
