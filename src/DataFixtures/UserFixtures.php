<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private \Faker\Generator $faker;

    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();

        $this->getUsers(2);

        $manager->flush();
    }

    private function getUsers(int $nbuser): void
    {
        // +3 à cause de ApiUserFixtures
        for ($i = 3; $i < $nbuser + 3; $i++) {
            $modulo = $i % 2;

            $user = new User();

            $user->setEmail($this->faker->email)
                // ->setPassword($this->passwordEncoder->encodePassword($user, "test")) <- EventPasswordEncoderSubscriber
                ->setPassword("test")
                ->setIsVerified(($modulo === 1));

            $this->manager->persist($user);

            $this->addReference("user{$i}", $user);
        }
    }
}
