<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SondageFixtures extends Fixture implements DependentFixtureInterface
{
    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->generateSondages(2);

        $manager->flush();
    }

    /**
     * @param int $nbSondage
     */
    private function generateSondages(int $nbSondage): void
    {
        for ($i = 0; $i < $nbSondage; $i++) {
            /**
             * @var Category $category
             * @type User $user
             */
            $category = $this->getReference("category{$i}");
            //$modulo = $i % 2;
            $modulo = 1;
            $user = $this->getReference("user{$modulo}");

            $sondage = (new Sondage())
                ->setQuestion("Question " . $i . " ?")
                ->setPropositions("Proposition " . $i . ".")
                ->setCreatedAt(new \DateTimeImmutable('2021-04-27T14:52:00'))
                ->setEditedAt(new \DateTime('2021-04-27T14:52:00'))
                ->setIsPublished(false)
                ->addCategory($category);

            if ($user instanceof User) {
                $sondage->setUser($user);
            }
            $this->addReference("sondage{$i}", $sondage);

            $this->manager->persist($sondage);
        }
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class,
        ];
    }
}
