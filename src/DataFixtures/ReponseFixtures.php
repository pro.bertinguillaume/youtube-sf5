<?php

namespace App\DataFixtures;

use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ReponseFixtures extends Fixture implements DependentFixtureInterface
{
    private ObjectManager $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->generateReponses(12);

        $manager->flush();
    }

    /**
     * @param int $nbReponse
     * @throws \JsonException
     */
    private function generateReponses(int $nbReponse): void
    {
        for ($i = 1; $i <= $nbReponse; $i++) {
            /**
             * @type Sondage $sondage
             * @type User $user
             * @type string $editedAt
             */
            $modulo = $i % 2;
            $sondage = $this->getReference("sondage{$modulo}");
            $user = $this->getReference("user{$modulo}");
            $editedAt = json_encode([(new \DateTime('2021-04-27T14:52:00'))->getTimestamp()], JSON_UNESCAPED_SLASHES);

            $reponse = (new Reponse())
                ->setValue("Reponse " . $i)
                ->setCreatedAt(new \DateTimeImmutable('2021-04-27T14:52:00'));

            if($sondage instanceof Sondage) {
                $reponse->setSondage($sondage);
            }

            if($user instanceof User) {
                $reponse->setUser($user);
            }

            if(is_string($editedAt)) {
                $reponse->setEditedAt($editedAt);
            }

            $this->addReference("reponse{$i}", $reponse);

            $this->manager->persist($reponse);
        }
    }

    /**
     * @return array{string}
     */
    public function getDependencies(): array
    {
        return [
            SondageFixtures::class,
        ];
    }
}
