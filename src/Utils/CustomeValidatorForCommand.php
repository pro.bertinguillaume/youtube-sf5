<?php

namespace App\Utils;

use Symfony\Component\Console\Exception\InvalidArgumentException;

class CustomeValidatorForCommand
{
    /**
     * Validates an email entered by the user in the CLI.
     *
     * @param string|null $emailEntered
     * @return string
     */
    public function validateEmail(?string $emailEntered): string
    {
        if (empty($emailEntered)) {
            throw new InvalidArgumentException("VEUILLEZ SAISIR UN EMAIL.");
        }

        if (!filter_var($emailEntered, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException("EMAIL SAISI INVALIDE.");
        }

        return $emailEntered;
    }

    /**
     * Validates a password entered by the user in the CLI.
     *
     * @param string|null $passwordEntered
     * @return string
     */
    public function validatePassword(?string $passwordEntered): string
    {
        if (empty($passwordEntered)) {
            throw new InvalidArgumentException("VEUILLEZ SAISIR UN MOT DE PASSE.");
        }

        // a récupérer de User
//        $passwordRegex = '';
//        if (!preg_match($passwordRegex, $passwordEntered)) {
//            throw new InvalidArgumentException("LE PASSWORD DOIT ...");
//        }

        return $passwordEntered;
    }
}
