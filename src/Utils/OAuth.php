<?php

namespace App\Utils;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

trait OAuth
{
    public function getDiscordRedirectUrl(UrlGeneratorInterface $urlGenerator): string
    {
        return $urlGenerator->generate('app_login', [
            'discord-oauth-provider' => true
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
