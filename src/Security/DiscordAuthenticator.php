<?php

namespace App\Security;

use _HumbugBox61bfe547a037\Nette\Utils\Json;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class DiscordAuthenticator extends AbstractGuardAuthenticator
{
    private CsrfTokenManagerInterface $csrfTokenManager;
    private DiscordUserProvider $discordUserProvider;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        CsrfTokenManagerInterface $csrfTokenManager,
        DiscordUserProvider $discordUserProvider,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->discordUserProvider = $discordUserProvider;
        $this->urlGenerator = $urlGenerator;
    }

    public function supports(Request $request): bool
    {
        return $request->query->has('discord-oauth-provider');
    }

    /**
     * @param Request $request
     * @return array<string, string|null>
     */
    public function getCredentials(Request $request): array
    {
        $state = $request->query->get('state');

        if (!$state || !$this->csrfTokenManager->isTokenValid(new CsrfToken('oauth-discord-oc', $state))) {
            throw new AccessDeniedException('abcde...');
        }

        return [
            'code' => $request->query->get('code')
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        if ($credentials === null) {
            return null;
        }

        return $this->discordUserProvider->loadUserFromDiscordOAuth($credentials['code']);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): JsonResponse
    {
        return new JsonResponse([
            'message' => 'D: Authentification refusée.'
        ], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): RedirectResponse
    {
        return new RedirectResponse($this->urlGenerator->generate('app_user_account_profile_home'));
    }

    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        return new JsonResponse([
            'message' => 'D: Authentification requise.'
        ], Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }
}
