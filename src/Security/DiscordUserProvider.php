<?php

namespace App\Security;

use App\Entity\User;
use App\Event\UserCreatedFromDiscordOAuthEvent;
use App\Repository\UserRepository;
use App\Service\PasswordGenerator;
use App\Utils\OAuth;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DiscordUserProvider implements UserProviderInterface
{
    use OAuth;

    private const DISCORD_ACCESS_TOKEN_ENDPOINT = 'https://discord.com/api/oauth2/token';
    private const DISCORD_USER_DATA_ENDPOINT = 'https://discordapp.com/api/users/@me';

    private EventDispatcherInterface $eventDispatcher;
    private HttpClientInterface $httpClient;
    private PasswordGenerator $passwordGenerator;
    private string $discordClientID;
    private string $discordClientSecret;
    private UrlGeneratorInterface $urlGenerator;
    private UserRepository $userRepository;

    /**
     * DiscordUserProvider constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param HttpClientInterface $httpClient
     * @param PasswordGenerator $passwordGenerator
     * @param string $discordClientID
     * @param string $discordClientSecret
     * @param UrlGeneratorInterface $urlGenerator
     * @param UserRepository $userRepository
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        HttpClientInterface $httpClient,
        PasswordGenerator $passwordGenerator,
        string $discordClientID,
        string $discordClientSecret,
        UrlGeneratorInterface $urlGenerator,
        UserRepository $userRepository
    )
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->httpClient = $httpClient;
        $this->passwordGenerator = $passwordGenerator;
        $this->discordClientID = $discordClientID;
        $this->discordClientSecret = $discordClientSecret;
        $this->urlGenerator = $urlGenerator;
        $this->userRepository = $userRepository;
    }

    public function loadUserFromDiscordOAuth(string $code): User
    {
        $accessToken = $this->getAccessToken($code);

        $discordUserData = $this->getUserInformations($accessToken);

        [
            'email' => $email,
            'id' => $discordID,
            'username' => $discordUserName
        ] = $discordUserData;

        $user = $this->userRepository->getUserFromDiscordOAuth($discordID, $discordUserName, $email);

        if (!$user) {
            $randomPassword = $this->passwordGenerator->generateRandomStrongPassword();

            $user = $this->userRepository->createUserFromDiscordOAuth(
                $discordID,
                $discordUserName,
                $email,
                $randomPassword
            );

            $this->eventDispatcher->dispatch(
                new UserCreatedFromDiscordOAuthEvent($email, $randomPassword),
                UserCreatedFromDiscordOAuthEvent::SEND_EMAIL_WITH_PASSWORD
            );
        }

        return $user;
    }

    public function loadUserByUsername(string $discordID): User
    {
        $user = $this->userRepository->findOneBy([
            'discordID' => $discordID
        ]);

        if (!$user) {
            throw new UsernameNotFoundException('D: Utilisateur inexistant');
        }

        return $user;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User || !$user->getDiscordID()) {
            throw new UnsupportedUserException();
        }

        /**
         * @var string $discordID
         */
        $discordID = $user->getDiscordID();

        return $this->loadUserByUsername($discordID);
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }

    private function getAccessToken(string $code): string
    {
        $redirectURL = $this->getDiscordRedirectUrl($this->urlGenerator);

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'body' => [
                'client_id' => $this->discordClientID,
                'client_secret' => $this->discordClientSecret,
                'code' => $code,
                'grant_type' => 'authorization_code',
                'redirect_uri' => $redirectURL,
                'scope' => 'identify email'
            ]
        ];

        $response = $this->httpClient->request('POST', self::DISCORD_ACCESS_TOKEN_ENDPOINT, $options);

        $data = $response->toArray();

        if (!$data['access_token']) {
            throw new ServiceUnavailableHttpException(null, 'D: l\'authentification via discord a échoué.');
        }

        return $data['access_token'];
    }

    /**
     * @param string $accessToken
     * @return array<string>
     */
    private function getUserInformations(string $accessToken): array
    {
        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}"
            ]
        ];

        $response = $this->httpClient->request('GET', self::DISCORD_USER_DATA_ENDPOINT, $options);

        $data = $response->toArray();

        if (!$data['email'] || !$data['id'] || !$data['username']) {
            throw new ServiceUnavailableHttpException(null, 'D: Api hors-service ou la reponse attendue a changé.');
        } elseif (!$data['verified']) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Le compte discord n\'est pas vérifié.');
        }

        return $data;
    }
}
