<?php

namespace App\Security;

use App\Repository\AuthLogRepository;
use Symfony\Component\HttpFoundation\RequestStack;

class BruteForceChecker
{
    private AuthLogRepository $authLogRepository;

    private RequestStack $requestStack;

    public function __construct(
        AuthLogRepository $authLogRepository,
        RequestStack $requestStack
    )
    {
        $this->authLogRepository = $authLogRepository;
        $this->requestStack = $requestStack;
    }

    /**
     * Add a failed authentication attempts and add a blacklisting according to the number of failed attempt
     *
     * @param string $emailEntered
     * @param string|null $userIp
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addFailedAuthAttempt(
        string $emailEntered,
        ?string $userIp
    ): void
    {
        if ($this->authLogRepository->isBlackListedWhitThisAttemptFailure($emailEntered, $userIp)) {
            $this->authLogRepository->addFailedOfAttempt($emailEntered, $userIp, true);
        } else {
            $this->authLogRepository->addFailedOfAttempt($emailEntered, $userIp);
        }
    }

    /**
     * Returns formatted end of blacklisting
     *
     * @return string
     * @throws \Exception
     */
    public function getEndOfBlackListing(): ?string
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return null;
        }

        $userIp = $request->getClientIp();
        $enteredEmail = $request->request->get('email');

        return $this->authLogRepository->getEndOfBlackLinsting($enteredEmail, $userIp);
    }
}
