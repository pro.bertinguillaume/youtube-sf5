<?php

namespace App\Security\Voter;

use App\Entity\Sondage;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class SondageVoter extends Voter
{
    public const CREATE = 'create';

    public const EDIT = 'edit';

    private const CAN_CREATE_ROLE = 'ROLE_WRITTER';

    private RoleHierarchyInterface $roleHierarchy;

    public function __construct(RoleHierarchyInterface $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (
            in_array(
                $attribute,
                [
                    self::CREATE,
                    self::EDIT,
                ]
            ) === false
        ) {
            return false;
        }

        if (
            $subject !== null
            && $subject instanceof Sondage === false
        ) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        /**
         * Note : l'utilisateur doit etre authentifié ici
         */
        if (!$user instanceof User) {
            return false;
        }

        if ($attribute === self::CREATE) {
            return $this->canCreate($user);
        }

        if (
            $attribute === self::EDIT
            && $subject instanceof Sondage
        ) {
            return $this->canEdit($subject, $user);
        }

        throw new \LogicException("The second argument passed to denyAccessUnlessGranted() method must be an instance of Sondage if rou want to edit a Sondage");
    }

    private function canCreate(User $user): bool
    {
        return in_array(
            self::CAN_CREATE_ROLE,
            $this->roleHierarchy->getReachableRoleNames($user->getRoles()),
            true
        );
    }

    private function canEdit(
        Sondage $sondage,
        User $user
    ): bool
    {
        return $sondage->getUser() === $user;
    }
}

