<?php

namespace App\Security;

use App\Entity\User;
use App\Event\AskForPasswordConfirmationEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\LogicException;
use Symfony\Component\Security\Core\Security;

class AskForPasswordConfirmation
{
    private EventDispatcherInterface $eventDispatcher;

    private RequestStack $requestStack;

    private Security $security;

    private SessionInterface $session;

    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * AskForPasswordConfirmation constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param RequestStack $requestStack
     * @param Security $security
     * @param SessionInterface $session
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        RequestStack $requestStack,
        Security $security,
        SessionInterface $session,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
        $this->security = $security;
        $this->session = $session;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Display the password confirmation modal for sensitive operation
     *
     * @return void
     */
    public function ask(): void
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            throw new LogicException("Hmm");
        }

        if (!$request->headers->get('Confirm-Identity-With-Password')) {
            $this->dispatchDisplayModalEvent();
        }

        $this->dispatchPasswordInvalidEventOrContinue($request);
    }

    /**
     * Dispatch modal event : display modal to confirm password
     *
     * @return void
     */
    private function dispatchDisplayModalEvent(): void
    {
        $this->eventDispatcher->dispatch(
            new AskForPasswordConfirmationEvents(),
            AskForPasswordConfirmationEvents::MODAL_DISPLAY
        );
    }

    /**
     * Dispatch password invalid event : invalid password
     * if valid, continue
     *
     * @return void
     */
    private function dispatchPasswordInvalidEventOrContinue(Request $request)
    {
        /**
         * @var string $json
         */
        $json = $request->getContent();

        $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

        if (!array_key_exists('password', $data)) {
            throw new HttpException(400, 'Le mot de passe doit être saisi');
        }

        $passwordEntered = $data['password'];

        /**
         * @var User $user
         */
        $user = $this->security->getUser();

        if (!$this->passwordEncoder->isPasswordValid($user, $passwordEntered)) {
            $this->invalidateSessionIfThreeInvalidConfirmPassword();

            $this->eventDispatcher->dispatch(
                new AskForPasswordConfirmationEvents(),
                AskForPasswordConfirmationEvents::PASSWORD_INVALID
            );
        }

        $this->session->remove('Password-Confirmation-Invalid');
    }

    /**
     * Invalide user's session if enter 3 invalid passwords
     *
     * @return void
     */
    private function invalidateSessionIfThreeInvalidConfirmPassword(): void
    {
        if (!$this->session->get('Password-Confirmation-Invalid')) {
            $this->session->set('Password-Confirmation-Invalid', 1);
        } else {
            $this->session->set('Password-Confirmation-Invalid', $this->session->get('Password-Confirmation-Invalid') + 1);

            if ($this->session->get('Password-Confirmation-Invalid') === 3) {
                $this->eventDispatcher->dispatch(
                    new AskForPasswordConfirmationEvents(),
                    AskForPasswordConfirmationEvents::SESSION_INVALIDATE
                );
            }
        }
    }
}
