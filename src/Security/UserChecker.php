<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    private RequestStack $requestStack;

    public function __construct(
        RequestStack $requestStack
    )
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof User) {
            return;
        }
    }

    /**
     * @param UserInterface $user
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof User) {
            return;
        }

        if (!$user->getIsVerified()) {
            /**
             * @var \DateTimeImmutable $accountMustBeVerifiedBefore
             */
            $accountMustBeVerifiedBefore = $user->getAccountMustBeVerifiedBefore();
            throw new CustomUserMessageAccountStatusException(
                "Votre compte n'est pas actif, veuillez consulter vos e-mails pour l'activer avant le {$accountMustBeVerifiedBefore->format('d/m/Y H\hi')}"
            );
        }

        if ($user->getIsGuardCheckIp() && !$this->isUserIpIsInWhitelist($user)) {
            throw new CustomUserMessageAccountStatusException(
                "Votre ip n'est pas présente dans les ips de la whitelist de cette utilisateur."
            );
        }
    }

    private function isUserIpIsInWhitelist(User $user): bool
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return false;
        }

        return in_array($request->getClientIp(), $user->getWhitelistedIpAdresses(), true);
    }
}
