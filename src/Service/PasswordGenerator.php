<?php

namespace App\Service;

class PasswordGenerator
{
    public function generateRandomStrongPassword(int $length = 20): string
    {
        $upperCaseLetters = $this->generateCaractersWithChartCodeRange([65, 90]); // A - Z
        $lowerCaseLetters = $this->generateCaractersWithChartCodeRange([97, 122]); // a - z
        $numbers = $this->generateCaractersWithChartCodeRange([48, 57]);
        $symbols = $this->generateCaractersWithChartCodeRange([33, 47, 58, 64, 91, 96, 123, 126]);

        $allCharacters = array_merge($upperCaseLetters, $lowerCaseLetters, $numbers, $symbols);

        $isArrayShuffled = shuffle($allCharacters);

        if (!$isArrayShuffled) {
            throw new \LogicException("shuffled a planté..");
        }

        return implode('', array_slice($allCharacters, 0, $length));
    }

    /**
     * @param array<int> $range
     * @return array<string>
     */
    private function generateCaractersWithChartCodeRange(array $range): array
    {
        if (count($range) === 2) {
            return range(chr($range[0]), chr($range[1]));
        }

//        $chunkAsciiCodes = array_chunk($range, 2);
//        $specialCaractersChunks = array_map(fn($range) => range(chr($range[0]), chr($range[1])), $chunkAsciiCodes);
//        $allSpecialCaracters = array_merge(...$specialCaractersChunks);
//          =
        return array_merge(...array_map(fn($range) => range(chr($range[0]), chr($range[1])), array_chunk($range, 2)));
    }
}
