<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\CustomeValidatorForCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddOneUserCommand extends Command
{
    protected static $defaultName = 'app:add-one-user';

    private CustomeValidatorForCommand $validator;

    private EntityManagerInterface $entityManager;

    private UserRepository $userRepository;

    private SymfonyStyle $io;

    public function __construct(
        CustomeValidatorForCommand $customeValidatorForCommand,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->validator = $customeValidatorForCommand;
    }

    protected function configure(): void
    {
        $this
            ->setDescription("Ajout d'un utilisateur")
            ->addArgument('email', InputArgument::REQUIRED, "L'email de l'utilisateur")
            ->addArgument('plainPassword', InputArgument::REQUIRED, "Le mot de passe de l'utilisateur en clair")
            ->addArgument('role', InputArgument::REQUIRED, "Le rôle de l'utilisateur")
            ->addArgument('isVerified', InputArgument::REQUIRED, "Le statut du compte utilisateur (actf ou non)");
    }

    /**
     * Executed after configure() to initialize properties based on the input arguments and options
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * Executed after initialize() and before execute()
     * Check if options/arguments are missing and ask them
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $this->io->section("AJOUT D'UN UTILISATEUR EN BASE DE DONNÉES");

        $this->enterEmail($input, $output);

        $this->enterPassword($input, $output);

        $this->enterRole($input, $output);

        $this->enterIsVerified($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var string $email
         */
        $email = $input->getArgument('email');

        /**
         * @var string $plainPassword
         */
        $plainPassword = $input->getArgument('plainPassword');

        /**
         * @var bool $isVerified
         */
        $isVerified = ($input->getArgument('isVerified') === 'ACTIF');

        /**
         * @var array<string> $role
         */
        $role = [$input->getArgument('role')];

        $user = new User();

        $user
            ->setEmail($email)
            ->setPassword($plainPassword) // Voir EventPasswordEncoderSubscriber
            ->setIsVerified($isVerified)
            ->setRoles($role);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->io->success("L'UTILISATEUR A ETE CREE.");

        return Command::SUCCESS;
    }

    /**
     * Sets the user email
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function enterEmail(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $emailQuestion = new Question("EMAIL DE L'UTILISATEUR : ");

        $emailQuestion->setValidator([$this->validator, 'validateEmail']);

        $email = $helper->ask($input, $output, $emailQuestion);

        if ($this->isUserAlreadyExists($email)) {
            throw new RuntimeException(
                sprintf("UTILISATEUR DEJA PRESENT EN BASE DE DONNEES AVEC L'EMAIL SUIVANT: %s", $email)
            );
        }

        $input->setArgument('email', $email);
    }

    private function isUserAlreadyExists(string $email): ?User
    {
        return $this->userRepository->findOneBy([
            'email' => $email
        ]);
    }

    /**
     * Sets the user password
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function enterPassword(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $passwordQuestion = new Question("MOT DE PASSE DE L'UTILISATEUR (HASHAGE ARGON2ID) : ");

        $passwordQuestion->setValidator([$this->validator, 'validatePassword']);

        // Hide password when typing
        $passwordQuestion
            ->setHidden(true)
            ->setHiddenFallback(false);

        $password = $helper->ask($input, $output, $passwordQuestion);

        $input->setArgument('plainPassword', $password);
    }

    /**
     * Sets the user role
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function enterRole(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $roleQuestion = new ChoiceQuestion(
            "SELECTION DU ROLE DE L'UTILISATEUR",
            [
                'ROLE_USER',
                'ROLE_WRITER',
                'ROLE_ADMIN',
                'ROLE_UNKNOW'
            ],
            'ROLE_USER'
        );

        $roleQuestion->setErrorMessage('ROLE UTILISATEUR INVALIDE : ');

        $role = $helper->ask($input, $output, $roleQuestion);

        $output->writeln("<info>ROLE UTILISATEUR PRIS EN COMPTE: {$role}</info>");

        $input->setArgument('role', $role);
    }

    /**
     * Sets the user statut (actif/inactif)
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function enterIsVerified(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $isVerifiedQuestion = new ChoiceQuestion(
            "SELECTION DU STATUS D'ACTIVATION DU COMPTE UTILISATEUR",
            ['INACTIF', 'ACTIF'],
            'ACTIF'
        );

        $isVerifiedQuestion->setErrorMessage("STATUT D'ACTIVATION DU COMPTE UTILISATEUR INVALIDE : ");

        $isVerified = $helper->ask($input, $output, $isVerifiedQuestion);

        $output->writeln("<info>STATUT D'ACTIVATION DU COMPTE: {$isVerified}</info>");

        $input->setArgument('isVerified', $isVerified);
    }
}
