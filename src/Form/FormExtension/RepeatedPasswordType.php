<?php

namespace App\Form\FormExtension;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepeatedPasswordType extends AbstractType
{
    public function getParent()
    {
        return RepeatedType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'type' => PasswordType::class,
            'invalid_message' => "Les mots de passe saisis ne correspondent pas.",
            'required' => true,
            'first_options' => [
                'label' => 'Mot de passe',
                'label_attr' => [
                    'title' => 'Annonce de la règle de mot de passe'
                ],
                'attr' => [
                    'class' => 'text-center',
//                        'pattern' => '',
                    'title' => 'Annonce de la règle de mot de passe',
                    'maxlength' => 255
                ]
            ],
            'second_options' => [
                'label' => 'Confirmer le mot de passe',
                'label_attr' => [
                    'title' => 'Confirmer le mot de passe'
                ],
                'attr' => [
                    'class' => 'text-center',
//                        'pattern' => '',
                    'title' => 'Annonce de la règle de mot de passe',
                    'maxlength' => 255
                ]
            ]]);
    }
}
