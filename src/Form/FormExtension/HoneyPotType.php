<?php

namespace App\Form\FormExtension;

use App\EventSubscriber\HoneyPotSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class HoneyPotType extends AbstractType
{
    private LoggerInterface $honeypotLogger;

    private RequestStack $requestStack;

    public function __construct(
        LoggerInterface $honeypotLogger,
        RequestStack $requestStack
    )
    {
        $this->honeypotLogger = $honeypotLogger;
        $this->requestStack = $requestStack;
    }

    protected const DELICIOUS_HONEY_CANDY_FOR_BOT = "phone";

    protected const FABULOUS_HONEY_CANDY_FOR_BOT = "fax";

    /**
     * @param FormBuilderInterface<callable> $builder
     * @param array<mixed> $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::DELICIOUS_HONEY_CANDY_FOR_BOT, TextType::class, $this->setHoneyPotFieldConfigured())
            ->add(self::FABULOUS_HONEY_CANDY_FOR_BOT, TextType::class, $this->setHoneyPotFieldConfigured())
            ->addEventSubscriber(new HoneyPotSubscriber($this->honeypotLogger, $this->requestStack));
    }

    /**
     * Return configuration for honey pot field
     *
     * @return array<mixed>
     */
    private function setHoneyPotFieldConfigured(): array
    {
        return [
            'row_attr' => ['class' => 'hp-field'],
            'attr' => [
                'autocomplete' => 'off',
                'tabindex' => '-1'
            ],
//            'data' => 'fake data',
            'mapped' => false,
            'required' => false
        ];
    }
}
