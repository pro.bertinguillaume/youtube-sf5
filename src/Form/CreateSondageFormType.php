<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Sondage;
use App\EventSubscriber\Form\CreateSondageFormSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateSondageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'question',
                TextType::class,
                [
                    'label' => "Question du sondage",
                    'required' => true
                ]
            )
            ->add(
                'categories',
                EntityType::class,
                [
                    'class' => Category::class,
                    'choice_label' => 'name',
                    'by_reference' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Catégorie du sondage',
                    'required' => true
                ]
            )
            ->add(
                'publish',
                SubmitType::class,
                [
                    'label' => 'Publier le sondage',
                    'attr' => [
                        'class' => 'btn btn-success'
                    ]
                ]
            )
        ->addEventSubscriber(new CreateSondageFormSubscriber());
//            ->add(
//                'picture',
//                FileType::class,
//                [
//                    'label' => 'Image du sondage',
//                    'mapped' => false,
//                    'required' => $options['is_edition'] ? false : true,
//                    'constraints' => [
//                        new Image([
//                            'maxSize' => '1M',
//                            'maxSizeMessage' => 'les poids maximum de l\'image doit être de {{ limit }} {{ suffix }}.'
//                        ])
//                    ],
//                ]
//            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sondage::class,
            'user_roles' => [],
            'is_edition' => false
        ]);
    }
}
