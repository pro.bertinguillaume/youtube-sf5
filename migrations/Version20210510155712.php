<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510155712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auth_logs (id INT AUTO_INCREMENT NOT NULL, auth_attempt_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', user_ip VARCHAR(255) DEFAULT NULL, email_entered VARCHAR(255) NOT NULL, is_successful_auth TINYINT(1) NOT NULL, start_of_black_listing DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', end_of_black_listing DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_remember_me_auth TINYINT(1) NOT NULL, deauthenticated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_sondage (category_id INT NOT NULL, sondage_id INT NOT NULL, INDEX IDX_B2A188E112469DE2 (category_id), INDEX IDX_B2A188E1BAF4AE56 (sondage_id), PRIMARY KEY(category_id, sondage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reponses (id INT AUTO_INCREMENT NOT NULL, sondage_id INT NOT NULL, user_id INT NOT NULL, value VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', edited_at VARCHAR(255) NOT NULL, INDEX IDX_1E512EC6BAF4AE56 (sondage_id), INDEX IDX_1E512EC6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sondages (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, question VARCHAR(500) NOT NULL, propositions LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', edited_at DATETIME DEFAULT NULL, is_published TINYINT(1) NOT NULL, INDEX IDX_7BCB3816A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, registered_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', account_must_be_verified_before DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', registration_token VARCHAR(255) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, account_verified_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', forgot_password_token VARCHAR(255) DEFAULT NULL, forgot_password_token_requested_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', forgot_password_token_must_be_verified_before DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', forgot_password_token_verified_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_guard_check_ip TINYINT(1) NOT NULL, whitelisted_ip_adresses JSON NOT NULL, discord_id VARCHAR(255) DEFAULT NULL, discord_user_name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_sondage ADD CONSTRAINT FK_B2A188E112469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_sondage ADD CONSTRAINT FK_B2A188E1BAF4AE56 FOREIGN KEY (sondage_id) REFERENCES sondages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reponses ADD CONSTRAINT FK_1E512EC6BAF4AE56 FOREIGN KEY (sondage_id) REFERENCES sondages (id)');
        $this->addSql('ALTER TABLE reponses ADD CONSTRAINT FK_1E512EC6A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE sondages ADD CONSTRAINT FK_7BCB3816A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category_sondage DROP FOREIGN KEY FK_B2A188E112469DE2');
        $this->addSql('ALTER TABLE category_sondage DROP FOREIGN KEY FK_B2A188E1BAF4AE56');
        $this->addSql('ALTER TABLE reponses DROP FOREIGN KEY FK_1E512EC6BAF4AE56');
        $this->addSql('ALTER TABLE reponses DROP FOREIGN KEY FK_1E512EC6A76ED395');
        $this->addSql('ALTER TABLE sondages DROP FOREIGN KEY FK_7BCB3816A76ED395');
        $this->addSql('DROP TABLE auth_logs');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_sondage');
        $this->addSql('DROP TABLE reponses');
        $this->addSql('DROP TABLE sondages');
        $this->addSql('DROP TABLE users');
    }
}
