/**
 * Update whitelist'IP adresses int the table and hides modal
 * @param {string|null} user_IP
 */
export default function updateWhiteListIpAdresses(user_IP) {
    const user_ip_addresses = document.body.querySelector('p[id="user_ip_adresses"]');

    user_ip_addresses.textContent = user_IP;
}
