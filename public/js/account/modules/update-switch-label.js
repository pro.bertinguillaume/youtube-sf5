/**
 * Updates the switch and it's label depending of the server response
 * @param {bool} is_guard_checking_IP
 */
export default function updateSwitchAndItsLabel(is_guard_checking_IP) {
    document.body.querySelector('label[for="check_user_ip_checkbox"]').textContent = is_guard_checking_IP ? "Active" : "Inactive";
    document.body.querySelector('input[id="check_user_ip_checkbox"]').checked = is_guard_checking_IP;
}
