import setCursorAtEnd from "./set-cursor-at-end.js";

export default function checkUserIpEntered(event) {
    const {currentTarget, key} = event;

    const allowed_keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', ':', '.', '/', ' ', '|', 'backspace', 'delete'];

    const pressed_key = key.toLowerCase();

    if (!allowed_keys.includes(pressed_key)) {
        event.stopPropagation();
    }

    if (pressed_key === ' ') {
        currentTarget.textContent += ' | ';
        setCursorAtEnd(currentTarget);
    }

    if (pressed_key === 'enter') {
        const text = currentTarget.textContent.trim();
        const last_charactere_allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', ''];

        if (!last_charactere_allowed.includes(text.charAt(text.length - 1))) {
            alert("L'addresse IP doit se terminer par un caractère héxadécimal: 0 à 9 ou A à F");
            throw new Error("L'addresse IP doit se terminer par un caractère héxadécimal: 0 à 9 ou A à F");
        }

        const ipv4_and_ipv6_regex = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;

        const ip_adresses = text.split(' | ');

        if (JSON.stringify(ip_adresses) !== JSON.stringify([""])) {
            ip_adresses.forEach(ip_adress => {
                if (!ipv4_and_ipv6_regex.test(ip_adress)) {
                    alert(`L'adresse IP suivante n'est pas valide : ${ip_adress}`);
                    throw new Error(`L'adresse IP suivante n'est pas valide : ${ip_adress}`);
                }
            });
        }

        return ip_adresses;
    }
}
